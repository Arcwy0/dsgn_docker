# DSGN++ docker

**DSGN++: Exploiting Visual-Spatial Relation for Stereo-based 3D Detectors**<br/>
Authors: Yilun Chen, Shijia Huang, Shu Liu, Bei Yu, Jiaya Jia

[[Paper]](https://arxiv.org/abs/2204.03039) &nbsp; [[Demo Video]](https://youtu.be/DdvX8WOG0iU)&nbsp;

### Data Preparation

(1) Download the [KITTI 3D object detection dataset](http://www.cvlibs.net/datasets/kitti/eval_object.php?obj_benchmark=3d) including velodyne, stereo images, labels for training, calibration matrices, and the [road plane](https://drive.google.com/file/d/1d5mq0RXRnvHPVeKx6Q612z0YRO1t2wAp/view?usp=sharing). The folders are organized as follows:

```
ROOT_PATH
├── data
│   ├── kitti
│   │   │── ImageSets
│   │   │── training
│   │   │   ├──calib & velodyne & label_2 & image_2 & image_3 & planes
│   │   │── testing
│   │   │   ├──calib & velodyne & image_2 & image_3
├── pcdet
├── mmdetection-v2.22.0
```

(2) Generate KITTI data list and joint Stereo-Lidar Copy-Paste database for training.

```
python -m pcdet.datasets.kitti.lidar_kitti_dataset create_kitti_infos
python -m pcdet.datasets.kitti.lidar_kitti_dataset create_gt_database_only --image_crops
```

Keep in mind that download and put the pre-computed road plane to ```./kitti/training/planes``` for precise copy-paste augmentation.

### Prerequisites

You should have CUDA (any version >=10.0) installed.
Also you should have [NVIDIA container toolkit](https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/latest/install-guide.html) with Docker.

### Performance and Model Zoo

#### ATTENTION

YOU NEED TO DOWNLOAD THE MODEL HERE AND PUT IT INTO DATA FOLDER AFTER CLONING THE REPO.

<table>
    <thead>
        <tr>
            <th>Methods</th>
            <!-- <th>Inference Time(s/im)</th> -->
            <th>Car</th>
            <th>Ped.</th>
            <th>Cyc.</th>
            <th>Models</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>DSGN++</td>
            <td>70.05</td>
            <td>39.42</td>
            <td>44.47</td>
            <td><a href="https://drive.google.com/file/d/1Z160fDx5abFZUARso1ixNJH-4UpjA4LI/view?usp=sharing"> GoogleDrive </a></td>
        </tr>
    </tbody>
</table>

### Installation with docker

(1) Clone repository.

```
git clone https://gitlab.com/Arcwy0/dsgn_docker
cd dsgn_docker
```

(2) COPY ImageSets folder to data/kitti/ folder. (```.gitignore``` file ignores data/kitti/, so you need to organize folders in the order given above.)

#### x86_x64

(3) Go to docker folder.

```
cd docker
```

In ```Dockerfile.x86_x64``` edit lines 77,79:

```
ARG TORCH_CUDA_ARCH_LIST='6.1;7.2;8.6+PTX'
RUN export TORCH_CUDA_ARCH_LIST='6.1;7.2;8.6+PTX'
```

They should include your GPU compute capability (if not included already). You can find it in the official [NVIDIA tables](https://developer.nvidia.com/cuda-gpus) or by using following lines of code in Python with PyTorch:

```
import torch
print(torch.cuda.get_device_capability())
```

(4) Build the image.

```
bash scripts/build.sh
```

(5) Use ```test.sh``` to run the container (it includes ```-d``` option for ```docker compose```, so later you will need to stop the container with ```docker stop dsgn2```) and ```into.sh``` to get into the container.

```
bash scripts/test.sh
bash scripts/into.sh
```

(6) Preprocess the data first. Note:this can be done only once since data folder is mounted shared volume in docker.

```
cd DSGN2/
python -m pcdet.datasets.kitti.lidar_kitti_dataset create_kitti_infos
python -m pcdet.datasets.kitti.lidar_kitti_dataset create_gt_database_only --image_crops

```

(7) 

#### aarch64

(3) Go to docker folder.

```
cd docker
```

(4) Build the image.

```
bash scripts/build.sh
```

(5) Change this line in ```test.yml```:

```
    target: /home/dsgn2/DSGN2/data
```

to

```
    target: /home/dsgn2/DSGN2_fork/data
```

(6) Use ```test.sh``` to run the container (it includes ```-d``` option for ```docker compose```, so later you will need to stop the container with ```docker stop dsgn2```) and ```into.sh``` to get into the container.

```
bash scripts/test.sh
bash scripts/into.sh
```

(7) Preprocess the data first. Note: This can be done only once, since the data folder is mounted as shared volume.

```
cd DSGN2/
python -m pcdet.datasets.kitti.lidar_kitti_dataset create_kitti_infos
python -m pcdet.datasets.kitti.lidar_kitti_dataset create_gt_database_only --image_crops

```

### Training and Inference

Change line #26 in the `configs/stereo/kitti_models/dsgn2.yaml`

```            
DB_INFO_PATH:
    - stereo_kitti_dbinfos_train.pkl
```
to 
```
DB_INFO_PATH:
    - kitti_infos_train.pkl
```

If you use pretrained model: inside the container, change lines according to comment in line #103 in the `configs/stereo/kitti_models/dsgn2.yaml`. Note: this has to be done every time the container is started.

Also cha

Train the model by

```
python -m torch.distributed.launch --nproc_per_node=4 tools/train.py \
    --launcher pytorch \
    --fix_random_seed \
    --workers 2 \
    --sync_bn \
    --save_to_file \
    --cfg_file ./configs/stereo/kitti_models/dsgn2.yaml \
    --tcp_port 8080 \
    --continue_train
```

Evaluating the model by

```
python -m torch.distributed.launch --nproc_per_node=4 tools/test.py \
    --launcher pytorch \
    --workers 2 \
    --save_to_file \
    --cfg_file ./configs/stereo/kitti_models/dsgn2.yaml \
    --exp_name default \
    --tcp_port 12345 \
    --ckpt_id 60 
```

These scripts are from the original paper. For evaluation you can use ```inference.sh``` script (which includes pretrained model, which you should have downloaded and put into your data folder).

```NCCL_DEBUG=INFO python3 tools/test.py --launcher pytorch --workers 2     --save_to_file --cfg_file ./configs/stereo/kitti_models/dsgn2.yaml --exp_name default --tcp_port 8080 --ckpt data/dsgn2_ep58.pth --ckpt_id 58```

The evaluation results can be found in the outputing model folder.

### Citation

If you find our work useful in your research, please consider citing:

```
@ARTICLE{chen2022dsgn++,
  title={DSGN++: Exploiting Visual-Spatial Relation for Stereo-Based 3D Detectors}, 
  author={Chen, Yilun and Huang, Shijia and Liu, Shu and Yu, Bei and Jia, Jiaya},
  journal={IEEE Transactions on Pattern Analysis and Machine Intelligence}, 
  year={2022}
}
```

### Acknowledgment

Our code is based on several released code repositories. We thank the great code from [LIGA-Stereo](https://github.com/xy-guo/LIGA-Stereo), [OpenPCDet](https://github.com/open-mmlab/OpenPCDet), [mmdetection](https://github.com/open-mmlab/mmdetection).

### Contact

If you get troubles or suggestions for this repository, please feel free to contact me (<chenyilun95@gmail.com>).
